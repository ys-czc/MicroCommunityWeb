/**
 * 英文包
 */
(function(window) {
    window.lang = {
        "systemName": "Yunshuགཞིས་ཁུལ་དོ་དམ་མ་ལག",
        "systemSimpleName": "Yunshu",
        "subSystemName": "གཞིས་ཁུལ་དོ་དམ་མ་ལག",
        "companyTeam": "java110ཚོགས་པ།",
        "welcome": "འཚམས་འདྲིར་ཕེབས་པར་དགའ་བསུ་ཞུ། Yunshuགཞིས་ཁུལ་དོ་དམ་མ་ལག",
        "signOut": "ཕྱིར་འཐེན།",
        "signIn": "ཐོ་འགོད།",
        "register": "ཐོ་འགོད།",
        "moreCommunity": "སྡེ་ཁུལ་དེ་བས་མང་བ།",
        "moreMsg": "གནས་ཚུལ་ཚང་མར་གཟིགས་པ།",
        "title": "Yunshuགཞིས་ཁུལ་དོ་དམ་མ་ལག",
        "noAccount": "ད་དུང་རྩིས་ཨང་མེད་དམ།",
        "areyouhasaccount": "རྩིས་ཐོ་ཡོད་སོང་ངམ།"
    }
})(window)